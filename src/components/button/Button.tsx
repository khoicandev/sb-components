import React from 'react';
import BsButton from 'react-bootstrap/Button';
import { ButtonProps } from 'react-bootstrap/Button';
import { ButtonVariant } from 'react-bootstrap/esm/types';

export interface BsButtonProps extends ButtonProps {
  /**
   * Variant of the button
   */
  variant: ButtonVariant;
  /**
   * Size of the button
   */
  size?: 'sm' | 'lg';
}

const Button = (props: BsButtonProps) => {
  return <BsButton 
            // style={{ margin: '2px' }} 
            {...props} 
          />;
};

export default Button;
