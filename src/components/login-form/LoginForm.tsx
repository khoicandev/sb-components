import React from 'react';
import Form from 'react-bootstrap/Form';
import { Controller, SubmitHandler, useForm } from 'react-hook-form';

import FormControl from '../form-control/FormControl';
import Button from '../button/Button';

export interface LoginFormData {
  username: string;
  password: string;
}

export interface LoginFormProps {
  /**
   * Form is loading
   */
  isLoading: boolean;
  /**
   * Function to run when click Login button
   */
  onLoginSubmit: (formData: LoginFormData) => void;
}

/**
 * The basic login form
 */
const LoginForm = ({ isLoading = false, onLoginSubmit }: LoginFormProps) => {
  const {
    control,
    formState: { errors },
    handleSubmit,
  } = useForm<LoginFormData>({
    defaultValues: {
      username: '',
      password: '',
    },
  });
  const onSubmit: SubmitHandler<LoginFormData> = (data) => onLoginSubmit(data);

  return (
    <Form noValidate onSubmit={handleSubmit(onSubmit)}>
      <Form.Group className="mb-3">
        <Controller
          name="username"
          rules={{ required: true }}
          control={control}
          render={({ field }) => (
            <FormControl
              type="text"
              placeholder="Username"
              disabled={isLoading}
              isInvalid={!!errors.username}
              {...field}
            />
          )}
        />
        {errors.username && (
          <Form.Control.Feedback type="invalid">
            Please enter username.
          </Form.Control.Feedback>
        )}
      </Form.Group>
      <Form.Group className="mb-3">
        <Controller
          name="password"
          control={control}
          render={({ field }) => (
            <FormControl
              type="password"
              placeholder="Password"
              disabled={isLoading}
              {...field}
            />
          )}
        />
      </Form.Group>
      <div className="d-grid gap-2">
        <Button
          type="submit"
          variant="primary"
          disabled={isLoading}
          // style={{ color: '#0d6e16' }}
        >
          {isLoading ? 'Logging in ...' : 'Login'}
        </Button>
      </div>
    </Form>
  );
};

export default LoginForm;
