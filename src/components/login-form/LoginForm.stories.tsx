import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { expect } from '@storybook/jest';
import { within, userEvent, waitFor } from '@storybook/testing-library';

import LoginForm from './LoginForm';

export default {
  title: 'Bootstrap/Composite Component/LoginForm',
  component: LoginForm,
  argTypes: {
    onLoginSubmit: { action: 'login clicked' }
  },
  parameters: {
    backgrounds: {
      values: [
        { name: 'red', value: '#f00' },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  decorators: [
    (Story) => (
      <div style={{ margin: '3em auto', maxWidth: '16em' }}>
        <Story />
      </div>
    ),
  ],
} as ComponentMeta<typeof LoginForm>;

const Template: ComponentStory<typeof LoginForm> = (args) => (
  <LoginForm {...args} />
);

export const Default = Template.bind({});
Default.args = {
  isLoading: false,
};

export const Loading = Template.bind({});
Loading.args = {
  ...Default.args,
  isLoading: true,
};

export const MissingUserNameWarning = Template.bind({});
MissingUserNameWarning.play = async ({ canvasElement }) => {
  const canvas = within(canvasElement);

  await userEvent.type(canvas.getByPlaceholderText('Password'), '12345678', {
    delay: 100,
  });

  await userEvent.click(canvas.getByRole('button'));

  await waitFor(() => {
    expect(canvas.getByText('Please enter username.')).toBeTruthy();
  });
};
