import React from 'react';
import logo from './logo.png';

const Logo = () => {
  return <img src={logo} alt="Logo" style={{ maxWidth: '14em' }} />;
};

export default Logo;
