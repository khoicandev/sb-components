import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import FormControl from './FormControl';

export default {
  title: 'Bootstrap/Single Component/Form Control',
  component: FormControl,
  argTypes: {
    size: {
      options: ['', 'sm', 'lg'],
      control: {
        type: 'select',
        labels: {
          '': 'Default',
          sm: 'Small',
          lg: 'Large',
        }
      }
    }
  },
} as ComponentMeta<typeof FormControl>

const Template: ComponentStory<typeof FormControl> = (args) => <FormControl {...args} />;

export const Default = Template.bind({});
Default.args = {
  placeholder: 'Input Placeholder',
  type: 'text',
};

export const SmallControl = Template.bind({});
SmallControl.args = {
  ...Default.args,
  size: 'sm'
};

export const LargeControl = Template.bind({});
LargeControl.args = {
  ...Default.args,
  size: 'lg'
};