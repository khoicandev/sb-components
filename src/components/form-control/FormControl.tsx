import React from 'react';
import BsFormControl from 'react-bootstrap/FormControl';
import { FormControlProps } from 'react-bootstrap/FormControl';

export interface BsFormControlProps extends FormControlProps {
  /**
   * Input type
   */
  type: string;
  /**
   * Input size
   */
  size?: 'sm' | 'lg';
  /**
   * Input placeholder
   */
  placeholder?: string;
}

/**
 * Form input to let user enter data
 */
const FormControl = React.forwardRef(({ type = 'text', ...props }: BsFormControlProps, ref) => {
  return <BsFormControl type={type} ref={ref} {...props} />;
});

export default FormControl;
