import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import LoginPage from './LoginPage';

export default {
  title: 'Bootstrap/Pages/Login',
  component: LoginPage,
  parameters: {
    layout: 'fullscreen',
    backgrounds: {
      default: 'twitter',
      values: [
        {
          name: 'twitter',
          value: '#fafafa',
        },
        {
          name: 'facebook',
          value: '#3b5998',
        },
      ],
    },
  },
} as ComponentMeta<typeof LoginPage>;

const Template: ComponentStory<typeof LoginPage> = () => <LoginPage />;

export const Default = Template.bind({});
Default.args = {};
