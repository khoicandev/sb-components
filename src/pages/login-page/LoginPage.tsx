import React from 'react';
import { Card, Col, Container, Row } from 'react-bootstrap';
import LoginForm, {
  LoginFormData,
} from '../../components/login-form/LoginForm';
import Logo from '../../components/logo/Logo';

const LoginPage = () => {
  const onLoginSubmit = (loginData: LoginFormData) => {
    console.log(loginData);
  };

  return (
    <Container style={{ height: '100vh', backgroundColor: '#fafafa' }}>
      <Row
        style={{ height: '100vh' }}
        className="justify-content-center align-items-center"
      >
        <Card body style={{ width: '21rem' }}>
          <Row className="justify-content-md-center mb-3">
            <Col md="auto">
              <Logo />
            </Col>
          </Row>
          <Row>
            <Col>
              <LoginForm isLoading={false} onLoginSubmit={onLoginSubmit} />
            </Col>
          </Row>
        </Card>
      </Row>
    </Container>
  );
};

export default LoginPage;
