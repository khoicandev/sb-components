# Storybook Sample

## Install Storybook

Storybook needs to be installed into an existing project. 

You can create a React project using CRA then install Storybook to it using the commands below.

```
yarn create react-app my-app --template typescript
npx sb init
```

Command to run storybook
```
yarn storybook
```

## Create Stories

### Sample Story Structure

```
import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import MyComponent from './MyComponent';

export default {
  title: 'ComponentName',
  component: MyComponent,
  parameters: {
    backgrounds: {
      values: [
        { name: 'red', value: '#f00' },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
  decorators: [
    (Story) => (
      <div style={{ margin: '3em auto', maxWidth: '16em' }}>
        <Story />
      </div>
    ),
  ],
} as ComponentMeta<typeof MyComponent>

const Template: ComponentStory<typeof MyComponent> = (args) => <MyComponent {...args} />;

export const DefaultStory = Template.bind({});
DefaultStory.args = {
  prop1: '1'
  prop2: 'a'
};

export const Story1 = Template.bind({});
Story1.args = {
  ...DefaultStory.args,
  prop2: 'b'
};

export const Story2 = Template.bind({});
Story2.args = {
  ...Default.args,
  prop2: 'c'
};
```

### Story Specs

Develop some components first, then create story file by the filename format `[component].stories.tsx`

Stories codes shound follow `Component Story Format (CSF)`:

* default export to describe component
* named export to describe stories
* control story hierarchy by `title` prop of default export

### Story Features

#### [Args](https://storybook.js.org/docs/react/writing-stories/args)

* Provide props data to component
* Args rendered in Controls panel can be changed on-the-fly
* Controls can be customized using argTypes
```
DefaultStory.args = {
  prop1: '1'
  prop2: 'a'
};
```

#### [Params](https://storybook.js.org/docs/react/writing-stories/parameters)

* Control Storybook features and addons (eg. background color selection, viewport)
```
  parameters: {
    backgrounds: {
      values: [
        { name: 'red', value: '#f00' },
        { name: 'green', value: '#0f0' },
        { name: 'blue', value: '#00f' },
      ],
    },
  },
```

#### [Decorators](https://storybook.js.org/docs/react/writing-stories/decorators)

* Rendering wrapper for story
```
  decorators: [
    (Story) => (
      <div style={{ margin: '3em auto', maxWidth: '16em' }}>
        <Story />
      </div>
    ),
  ],
```

#### [Loader](https://storybook.js.org/docs/react/writing-stories/loaders)

* Use to load data for story and decorators
* Run before the story rendered

#### [Play functions](https://storybook.js.org/docs/react/writing-stories/play-function)

* Codes that executes after story rendered
* Interact with components
* Userful for writing test scenarios

## Documentation

* DocsPage addon is installed by default when init storybook
* Generate docs based on comments of components, fields ...
* Create custom document by writing [MDX story](https://storybook.js.org/docs/react/writing-docs/mdx)

## Testing

### [Visual test](https://storybook.js.org/docs/react/writing-tests/visual-testing)

* Capture screenshot of stories and compare commit-by-commit
* Use cloud-based Chromatic or local addon StoryShots 

### [Accessibility test](https://storybook.js.org/docs/react/writing-tests/accessibility-testing)

* Install `addon-a11y` to use
* Check for usable color contrast, touch friendly ...

### [Interaction test](https://storybook.js.org/docs/react/writing-tests/interaction-testing)

* Install [addon-interactions](https://storybook.js.org/addons/@storybook/addon-interactions) to use
* Wirting play functions to simulate user actions, and test codes to check UI updated correctly

### [Snapshot test](https://storybook.js.org/docs/react/writing-tests/snapshot-testing)

* Compare DOM to known baseline
* Install `addon-storyshots` to use
* `yarn test storybook.test.ts`

## Sharing

* Publish to Chromatic: `npx chromatic --project-token=...`
    *  Chromatic run tests automatically when published
    *  Chromatic provided advanced features like: review with others, get feedbacks, changes approval
* Publish to local: `yarn build-storybook`
    *  Static website, can be deployed to any web server