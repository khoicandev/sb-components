<map version="freeplane 1.9.8">
<!--To view this file, download free mind mapping software Freeplane from https://www.freeplane.org -->
<node TEXT="Storybook" LOCALIZED_STYLE_REF="AutomaticLayout.level.root" FOLDED="false" ID="Freemind_Link_1513112588" CREATED="1153430895318" MODIFIED="1641965309450">
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<hook NAME="MapStyle" background="#ffffff">
    <properties fit_to_viewport="false" show_icon_for_attributes="true" show_note_icons="true" edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" associatedTemplateLocation="template:/light_super_hero_template.mm"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ID="ID_249848891" ICON_SIZE="12 pt" FORMAT_AS_HYPERLINK="false" COLOR="#000000" BACKGROUND_COLOR="#fff024" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_WIDTH_LIKE_EDGE="false" BORDER_WIDTH="1.9 px" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0" BORDER_DASH_LIKE_EDGE="true" BORDER_DASH="SOLID" VGAP_QUANTITY="3 pt">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" DASH="" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_249848891" STARTARROW="DEFAULT" ENDARROW="NONE"/>
<font NAME="SansSerif" SIZE="10" BOLD="false" STRIKETHROUGH="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#09387a" WIDTH="3" DASH="SOLID"/>
<richcontent CONTENT-TYPE="plain/auto" TYPE="DETAILS"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="plain/auto"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details" COLOR="#fff024" BACKGROUND_COLOR="#000000">
<font BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#f6f9a1" TEXT_ALIGN="LEFT">
<icon BUILTIN="clock2"/>
<font SIZE="10" ITALIC="true"/>
<edge COLOR="#000000"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" COLOR="#000000" BACKGROUND_COLOR="#f5131f" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#f5131f"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.important" ID="ID_254323574" STYLE="narrow_hexagon" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#c3131f">
<icon BUILTIN="yes"/>
<arrowlink COLOR="#c3131f" TRANSPARENCY="255" DESTINATION="ID_254323574"/>
<font SIZE="11" BOLD="true"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#ffffff" BACKGROUND_COLOR="#000000" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="10 pt" SHAPE_VERTICAL_MARGIN="10 pt" BORDER_WIDTH="3.1 px" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#2c2b29" BORDER_DASH_LIKE_EDGE="true">
<font NAME="Ubuntu" SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#ffffff" BACKGROUND_COLOR="#09387a" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_COLOR="#2c2b29">
<font NAME="Ubuntu" SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#ffffff" BACKGROUND_COLOR="#126cb3" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#000000" BACKGROUND_COLOR="#d4e226" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#000000" BACKGROUND_COLOR="#fff024" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="11"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5" BACKGROUND_COLOR="#fff024" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="11"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6" BACKGROUND_COLOR="#fff024" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7" BACKGROUND_COLOR="#fff024" BORDER_COLOR="#f0f0f0">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8" BACKGROUND_COLOR="#fff024" BORDER_COLOR="#f0f0f0">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9" BORDER_COLOR="#f0f0f0">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10" BORDER_COLOR="#f0f0f0">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11" BORDER_COLOR="#f0f0f0">
<font SIZE="9"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Why" POSITION="right" ID="ID_14964750" CREATED="1641965310052" MODIFIED="1642131847019">
<node TEXT="Develop UI faster by isolating components" ID="ID_95389598" CREATED="1641979588760" MODIFIED="1641979610315">
<node TEXT="Work on one component at a time" ID="ID_1668305714" CREATED="1641979613884" MODIFIED="1641979622765"/>
<node TEXT="Without the needs to start up a complex dev stack" ID="ID_1395178174" CREATED="1641979650978" MODIFIED="1641979681218"/>
<node TEXT="CDD: Component-Driven Development" ID="ID_1575635577" CREATED="1642170731456" MODIFIED="1642170741374">
<node TEXT="builds UIs from the “bottom-up”" ID="ID_309018376" CREATED="1642170757392" MODIFIED="1642170759170"/>
<node TEXT="starting with components and ending with screens" ID="ID_1609640336" CREATED="1642170763060" MODIFIED="1642170776711"/>
</node>
</node>
<node TEXT="Documenting" ID="ID_1813644760" CREATED="1641979623259" MODIFIED="1642600870905"/>
<node TEXT="Test to prevent bugs" ID="ID_1901499121" CREATED="1641979726232" MODIFIED="1642600881057"/>
</node>
<node TEXT="What" POSITION="right" ID="ID_32463387" CREATED="1641965315676" MODIFIED="1641965317417">
<node TEXT="Create Story" ID="ID_594061651" CREATED="1641996461171" MODIFIED="1641998509633">
<node TEXT="Capture rendered state of an UI component" ID="ID_1578513145" CREATED="1641996476020" MODIFIED="1641996495491"/>
<node TEXT="Multiple stories for an UI component to describe interesting states" ID="ID_467022376" CREATED="1641996496090" MODIFIED="1641996552317"/>
</node>
<node TEXT="Documentation" ID="ID_1532361133" CREATED="1642000519743" MODIFIED="1642000529556">
<node TEXT="Usage guidelines, design system sites" ID="ID_957458208" CREATED="1642000532914" MODIFIED="1642044896993"/>
</node>
<node TEXT="Tests" ID="ID_1207934033" CREATED="1642219887468" MODIFIED="1642219893664"/>
<node TEXT="Publish storybook" ID="ID_1877890262" CREATED="1642219818560" MODIFIED="1642219836381">
<node TEXT="gather feedbacks from stakeholders" ID="ID_1378834683" CREATED="1642219821695" MODIFIED="1642219843161"/>
<node TEXT="without touching code or setup local dev env" ID="ID_322023359" CREATED="1642219860979" MODIFIED="1642219875890"/>
</node>
</node>
<node TEXT="How" POSITION="left" ID="ID_1331736730" CREATED="1641965312887" MODIFIED="1641965315424">
<node TEXT="Install Storybook" ID="ID_850205529" CREATED="1641999265454" MODIFIED="1641999270526">
<node TEXT="Install to existing project" ID="ID_1810766541" CREATED="1641999298288" MODIFIED="1642074372304"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      yarn create react-app my-app --template typescript
    </p>
    <p>
      npx sb init
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="Create Story" ID="ID_1388317343" CREATED="1641999731788" MODIFIED="1641999734328">
<node TEXT="specs" ID="ID_892517597" CREATED="1642139959932" MODIFIED="1642169292256">
<node TEXT="Create alongside with component file" ID="ID_185879738" CREATED="1642000084594" MODIFIED="1642000091929">
<node TEXT="[component].stories.tsx" ID="ID_1856800436" CREATED="1641999746371" MODIFIED="1642074442426"/>
</node>
<node TEXT="CSF: Component Story Format" ID="ID_259102304" CREATED="1641999781033" MODIFIED="1642071477731">
<node TEXT="default export to describe component" ID="ID_853422762" CREATED="1642000012401" MODIFIED="1642000024716"/>
<node TEXT="named export to describe stories" ID="ID_208465233" CREATED="1642000025390" MODIFIED="1642000077575"/>
</node>
<node TEXT="naming and hierarchy" ID="ID_764937666" CREATED="1642074954693" MODIFIED="1642074965579">
<node ID="ID_1467779281" CREATED="1642075008526" MODIFIED="1642171008978"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      naming: <b>title</b>&nbsp;prop in export default
    </p>
  </body>
</html>
</richcontent>
<font BOLD="false"/>
</node>
<node ID="ID_949968249" CREATED="1642075025985" MODIFIED="1642169850783"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      create navigation group: forward-slash in <b>title </b>prop
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="features" ID="ID_1761608658" CREATED="1642139898030" MODIFIED="1642169277705">
<node ID="ID_1515139904" CREATED="1642068042666" MODIFIED="1642399381338">
<icon BUILTIN="full-1"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>args</b>
    </p>
  </body>
</html>
</richcontent>
<node TEXT="provide props to components" ID="ID_459767780" CREATED="1642068580397" MODIFIED="1642140174055"/>
<node TEXT="can reuse between stories to reduce duplicated codes" ID="ID_1779758066" CREATED="1642071759125" MODIFIED="1642400812441"/>
<node TEXT="args can be changed in Controls panel" ID="ID_545307336" CREATED="1642068567858" MODIFIED="1642140413354"/>
<node TEXT="customize args control by argTypes" ID="ID_253413847" CREATED="1642400815463" MODIFIED="1642400825891"/>
</node>
<node ID="ID_1310707968" CREATED="1642071029874" MODIFIED="1642215465709">
<icon BUILTIN="full-2"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>params</b>
    </p>
  </body>
</html>
</richcontent>
<node TEXT="story metadata: control Storybook feature and addons" ID="ID_1521983062" CREATED="1642071050018" MODIFIED="1642071419801"/>
<node TEXT="eg: values for background toolbar" ID="ID_275579898" CREATED="1642071843971" MODIFIED="1642071856204"/>
</node>
<node ID="ID_1036573085" CREATED="1642072445640" MODIFIED="1642215467975">
<icon BUILTIN="full-3"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>decorators</b>
    </p>
  </body>
</html>
</richcontent>
<node TEXT="rendering wrapper for story" ID="ID_1940515638" CREATED="1642072459304" MODIFIED="1642072480545"/>
<node TEXT="can be used to provide context component" ID="ID_1255049282" CREATED="1642140219080" MODIFIED="1642485390346"/>
</node>
<node TEXT="loader" ID="ID_1008846367" CREATED="1642074148625" MODIFIED="1642215470135">
<icon BUILTIN="full-4"/>
<node TEXT="load data for story and decorator" ID="ID_587472800" CREATED="1642074575571" MODIFIED="1642074592686"/>
<node TEXT="run before story render" ID="ID_1445850638" CREATED="1642074876297" MODIFIED="1642074882718"/>
</node>
<node TEXT="play functions" ID="ID_715914231" CREATED="1642074144515" MODIFIED="1642215472084">
<icon BUILTIN="full-5"/>
<node TEXT="codes execute after story renders" ID="ID_833274944" CREATED="1642074190728" MODIFIED="1642074244697"/>
<node TEXT="interact with component" ID="ID_1264451938" CREATED="1642074500438" MODIFIED="1642074508374"/>
<node TEXT="useful for test scenarios" ID="ID_1856738858" CREATED="1642074288375" MODIFIED="1642074525353"/>
</node>
</node>
<node TEXT="cases" ID="ID_859228622" CREATED="1642560937432" MODIFIED="1642560939673">
<node TEXT="for single component" ID="ID_1630166007" CREATED="1642131885913" MODIFIED="1642175028396">
<font ITALIC="false"/>
<node TEXT="develop component" ID="ID_1847612673" CREATED="1642175029655" MODIFIED="1642175057332"/>
<node TEXT="develop story codes" ID="ID_408954724" CREATED="1642175040276" MODIFIED="1642175061426">
<node TEXT="using args to provide props to component" ID="ID_729933798" CREATED="1642175088844" MODIFIED="1642175203457"/>
<node TEXT="build multiple stories for component" ID="ID_1597677170" CREATED="1642175211510" MODIFIED="1642175230635"/>
</node>
</node>
<node TEXT="for composite component" ID="ID_854965762" CREATED="1642169074807" MODIFIED="1642169095253">
<node TEXT="the same like single component" ID="ID_765059553" CREATED="1642175670495" MODIFIED="1642175727885"/>
<node TEXT="develop story codes" ID="ID_1382858949" CREATED="1642177326726" MODIFIED="1642177331557">
<node TEXT="using subcomponent stories as children in args for story reuse" ID="ID_592481469" CREATED="1642177333955" MODIFIED="1642177610323"/>
</node>
</node>
<node TEXT="for pages, screens" ID="ID_1591300908" CREATED="1642075100181" MODIFIED="1642169306597">
<node TEXT="the same like single component" ID="ID_1400759351" CREATED="1642175670495" MODIFIED="1642175727885"/>
</node>
</node>
</node>
<node TEXT="Documentation" ID="ID_1598198727" CREATED="1642000593812" MODIFIED="1642000598345">
<node TEXT="DocsPage" ID="ID_426628635" CREATED="1642000654033" MODIFIED="1642000775308">
<node TEXT="Installed by default when init Storybook" ID="ID_1621988518" CREATED="1642000776581" MODIFIED="1642000789481"/>
<node TEXT="Default template document provided" ID="ID_849290069" CREATED="1642000984488" MODIFIED="1642042167368"/>
<node TEXT="Customize document by creating mdx story" ID="ID_1588094055" CREATED="1642000789750" MODIFIED="1642563987387" LINK="https://storybook.js.org/docs/react/writing-docs/mdx">
<node TEXT="Difficult in code formatting &amp; code experience" ID="ID_1038904870" CREATED="1642563806638" MODIFIED="1642563909223"/>
</node>
</node>
</node>
<node TEXT="Testing" ID="ID_1241260128" CREATED="1642178336463" MODIFIED="1642178338446">
<node TEXT="Visual test" ID="ID_1582206335" CREATED="1642216024331" MODIFIED="1642216683004">
<font ITALIC="false"/>
<node TEXT="capture screenshot of stories" ID="ID_472975111" CREATED="1642216119383" MODIFIED="1642216193887"/>
<node TEXT="compare commit-by-commit" ID="ID_1927624126" CREATED="1642216194320" MODIFIED="1642216200861"/>
<node TEXT="Chromatic - cloud based" ID="ID_1097993722" CREATED="1642216241973" MODIFIED="1642569041279"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      yarn add --dev chromatic
    </p>
    <p>
      npx chromatic --project-token=7471a87ad62b
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="StoryShots - local" ID="ID_1895484000" CREATED="1642216251168" MODIFIED="1642216267084"/>
</node>
<node TEXT="Accessibility test" ID="ID_588428484" CREATED="1642216684084" MODIFIED="1642571151163"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      yarn add --dev @storybook/addon-a11y
    </p>
  </body>
</html>
</richcontent>
<node TEXT="usable color contrast" ID="ID_294345089" CREATED="1642216690624" MODIFIED="1642216707079"/>
<node TEXT="touch friendly ..." ID="ID_813794336" CREATED="1642216808609" MODIFIED="1642216812301"/>
</node>
<node TEXT="Interaction test" ID="ID_1632130633" CREATED="1642217946862" MODIFIED="1642582538768"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      https://storybook.js.org/docs/react/writing-tests/interaction-testing
    </p>
  </body>
</html>
</richcontent>
<node TEXT="supply initial props" ID="ID_1631180219" CREATED="1642217952607" MODIFIED="1642217958684"/>
<node TEXT="simulate user interaction by play functions" ID="ID_1342790754" CREATED="1642217959956" MODIFIED="1642218259903"/>
<node TEXT="test if state and UI updated correctly" ID="ID_1171703075" CREATED="1642218022940" MODIFIED="1642218077389"/>
</node>
<node TEXT="Snapshot test" ID="ID_1360405091" CREATED="1642218245487" MODIFIED="1642582547742"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      https://storybook.js.org/docs/react/writing-tests/snapshot-testing
    </p>
  </body>
</html>
</richcontent>
<node TEXT="compare rendered DOM against known baseline" ID="ID_294609258" CREATED="1642218753701" MODIFIED="1642582520214"/>
</node>
</node>
<node TEXT="Sharing" ID="ID_1393533340" CREATED="1642178345087" MODIFIED="1642178346844">
<node TEXT="Publish" ID="ID_650419579" CREATED="1642218848782" MODIFIED="1642218851168">
<node TEXT="publish to static web files or Chromatic" ID="ID_870594362" CREATED="1642218968538" MODIFIED="1642584781221"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      npx chromatic --project-token=
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Advanced scenarios" ID="ID_1195279498" CREATED="1642219378986" MODIFIED="1642219385031">
<node TEXT="Embed" ID="ID_708101656" CREATED="1642218852082" MODIFIED="1642218853659">
<node TEXT="embed a story deployed in Chromatic directly to a page" ID="ID_1451412085" CREATED="1642219411264" MODIFIED="1642583350127"/>
</node>
<node TEXT="Composition" ID="ID_613345445" CREATED="1642218855187" MODIFIED="1642218857723">
<node TEXT="combine multiple storybooks in Chromatic into one" ID="ID_1393436642" CREATED="1642219440737" MODIFIED="1642583354075"/>
</node>
</node>
</node>
</node>
</node>
</map>
